import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ActionDialogComponent } from 'src/app/components/action-dialog/action-dialog.component';
import { Produto } from 'src/app/models/produto.model';
import { ActiveProductService } from 'src/app/services/active-product.service';
import { DatabaseService } from 'src/app/services/database.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})
export class ListagemComponent implements OnInit, OnDestroy {

  displayedColumns = ['nome', 'unidadeMedida', 'quantidade', 'preco', 'perecivel', 'dataValidade', 'dataFabricacao'];
  dataSource: MatTableDataSource<Produto> = null;

  searchQuery = '';
  isSearching = false;

  private inputValueChanged: Subject<string> = new Subject<string>();
  private subscription: Subscription;
  private debounceTime = 500;


  constructor(
    public dialog: MatDialog,
    private loading: LoadingService,
    private router: Router,
    private activeProduct: ActiveProductService,
    private database: DatabaseService) { }

  ngOnInit() {
    this.getItems();

    this.subscription = this.inputValueChanged
      .pipe(
        debounceTime(this.debounceTime),
      )
      .subscribe((searchString) => {
        this.applyFilter(searchString);
      });
  }

  inputChanged(event: Event) {
    this.isSearching = true;
    const filterValue = (event.target as HTMLInputElement).value;
    this.inputValueChanged.next(filterValue);
  }

  applyFilter(searchString: string) {
    this.dataSource.filter = searchString.trim().toLowerCase();
    this.isSearching = false;
  }

  onClickItem($event) {
    const dialogRef = this.dialog.open(ActionDialogComponent);

    dialogRef.afterClosed().subscribe(action => {
      if (action === 'delete') {
        this.deleteItem($event);
      } else {
        this.editItem($event);
      }
    });
  }

  getItems() {
    setTimeout(() => this.loading.isLoading = true);
    setTimeout(() => {
      this.dataSource = new MatTableDataSource<Produto>(this.database.getAllProducts());
      this.loading.isLoading = false;
    }, 2000);
  }

  editItem(produto: Produto) {
    this.activeProduct.activeProduct = produto;
    this.router.navigate(['formulario']);
  }

  deleteItem(produto: Produto) {
    this.database.delete(produto);
    this.getItems();
  }

  addNewItem() {
    this.router.navigate(['formulario']);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
