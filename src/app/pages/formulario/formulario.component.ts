import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UnidadeMedida } from 'src/app/enums/unidade-medida.enum';
import { dateConverter } from 'src/app/helpers/date-converter.helper';
import { Produto } from 'src/app/models/produto.model';
import { ActiveProductService } from 'src/app/services/active-product.service';
import { DatabaseService } from 'src/app/services/database.service';
import { validDateValidator } from 'src/app/validators/valid-date.validator';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public produtoForm: FormGroup;

  private nomeTamanhoMaximo = 50;

  constructor(
    private formBuilder: FormBuilder,
    private database: DatabaseService,
    private activeProduct: ActiveProductService,
    private router: Router) { }

  ngOnInit(): void {
    this.produtoForm = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.maxLength(this.nomeTamanhoMaximo)]],
      unidadeMedida: ['', Validators.required],
      quantidade: [{ value: '', disabled: true }],
      preco: ['', Validators.required],
      perecivel: [false],
      dataValidade: [{ value: '', disabled: true }, validDateValidator()],
      dataFabricacao: ['', [validDateValidator(), Validators.required]]
    });

    this.observeChangesAndPerformValidations();
    this.setUpdateOrCreate();
  }


  formControlHasError(formControlName: string) {
    if (this.produtoForm.get(formControlName).errors) {
      return true;
    }
    return false;
  }

  getErrorMessage(formControlName: string, fieldFormattedName: string) {
    const errors = this.produtoForm.get(formControlName).errors;
    const errorType = Object.keys(errors)[0];

    switch (errorType) {
      case "required":
        return `${fieldFormattedName} é obrigatório.`
      case "dataInvalida":
        return `${fieldFormattedName} inválida.`;
      case "maxlength":
        return `Tamanho máximo ${this.nomeTamanhoMaximo} caracteres.`;
      case "dataMaiorQueValidade":
        return `Fabricação maior que validade.`;
      default:
        return 'Erro';
    }
  }

  getFormattedMeasurement(unidade: string) {
    return `${unidade} ${UnidadeMedida[unidade]}`;
  }

  getMeasurement(unidade: string) {
    return UnidadeMedida[unidade];
  }

  getMeasurementValues() {
    return Object.keys(UnidadeMedida);
  }

  getQuantitySuffix() {
    const unityValue = this.produtoForm.get('unidadeMedida').value;

    switch (unityValue) {
      case (UnidadeMedida.LITRO):
        return ` ${UnidadeMedida.LITRO}`;
      case (UnidadeMedida.QUILOGRAMA):
        return ` ${UnidadeMedida.QUILOGRAMA}`;
      case (UnidadeMedida.UNIDADE):
        return ` ${UnidadeMedida.UNIDADE}`;
      default:
        return '';
    }
  }

  getQuantityMask() {
    const unityValue = this.produtoForm.get('unidadeMedida').value;

    switch (unityValue) {
      case (UnidadeMedida.LITRO):
      case (UnidadeMedida.QUILOGRAMA):
        return 'separator.3';
      case (UnidadeMedida.UNIDADE):
        return '0*';
      default:
        return '';
    }
  }

  verificaValidadeProduto() {
    if (this.produtoForm.get('dataValidade').valid || this.produtoForm.get('dataValidade').value !== '') {
      const now = Date.parse(new Date().toString());

      if (dateConverter(this.produtoForm.get('dataValidade').value) < now) {
        return '(Produto vencido)';
      }
    }
    return '';
  }

  onSubmit() {
    if (this.produtoForm.valid) {
      const p = this.produtoForm.value;
      
      // foi colocado um "ou ''" para que o atributo de data de validade
      // mesmo desabilitado no form entre na 'persistência'
      // e não quebre a aplicação na hora de montar o form de update
      const produto = new Produto(p.nome, p.unidadeMedida,
        p.quantidade, p.preco, p.perecivel, p.dataValidade || '', p.dataFabricacao);

      if(this.activeProduct.activeProduct){
        produto.id = this.activeProduct.activeProduct.id;
        this.database.update(produto);
        this.activeProduct.activeProduct = null;
      } else {
        this.database.saveProduct(produto);
      }
      this.router.navigate(['']);
    }
  }

  cancelAction() {
    this.router.navigate(['']);
  }

  private setUpdateOrCreate() {
    if (this.activeProduct.activeProduct) {
      const productToUpdate = Object.assign({}, this.activeProduct.activeProduct);
      delete productToUpdate.id;
      this.produtoForm.setValue(productToUpdate);
    }
  }

  private observeChangesAndPerformValidations() {
    this.produtoForm.get('perecivel').valueChanges.subscribe(
      value => {
        if (!value) {
          this.produtoForm.get('dataValidade').setErrors(null);
          this.produtoForm.get('dataValidade').setValue('');
          this.produtoForm.get('dataValidade').disable();
        } else {
          this.produtoForm.get('dataValidade').enable();
          this.produtoForm.get('dataValidade').setErrors({ required: true });
        }
      });

    this.produtoForm.get('unidadeMedida').valueChanges.subscribe(
      _ => {
        this.produtoForm.get('quantidade').enable();
      });

    this.produtoForm.get('dataValidade').valueChanges.subscribe(
      _ => {
        this.validateDataFabricacao();
        this.verificaValidadeProduto();
      });

    this.produtoForm.get('dataFabricacao').valueChanges.subscribe(
      _ => {
        this.validateDataFabricacao();
      });
  }

  private validateDataFabricacao() {
    if (this.produtoForm.get('dataValidade').valid || this.produtoForm.get('dataValidade').value !== '') {
      const dataFab = dateConverter(this.produtoForm.get('dataFabricacao').value);
      const dataVal = dateConverter(this.produtoForm.get('dataValidade').value);

      if (dataFab > dataVal) {
        this.produtoForm.get('dataFabricacao').setErrors({ dataMaiorQueValidade: true });
      } else {
        this.produtoForm.get('dataFabricacao').setErrors(null);
      }
    }
  }
}
