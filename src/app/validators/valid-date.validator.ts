import { AbstractControl, ValidatorFn } from '@angular/forms';
import { dateConverter } from '../helpers/date-converter.helper';

export function validDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        if (!control.value || control.value === '') {
            return null;
        }

        const newDate = dateConverter(control.value);

        if (isNaN(newDate)) {
            return { dataInvalida: true };
        } else {
            return null;
        }
    };
}