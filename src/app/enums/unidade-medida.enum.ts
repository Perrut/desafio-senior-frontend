export enum UnidadeMedida {
    LITRO = 'lt',
    QUILOGRAMA = 'kg',
    UNIDADE = 'un'
}