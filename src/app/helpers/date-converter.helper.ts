export function dateConverter(dateString: string) {
    const splittedDate = dateString.split('/');
    const correctDate = `${splittedDate[1]}/${splittedDate[0]}/${splittedDate[2]}`;
    const newDate = Date.parse(correctDate);

    return newDate;
}