import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-action-dialog',
  templateUrl: './action-dialog.component.html',
  styleUrls: ['./action-dialog.component.css']
})
export class ActionDialogComponent implements OnInit {

  public deleteAction = false;

  constructor(public dialogRef: MatDialogRef<ActionDialogComponent>) { }

  ngOnInit(): void {
  }

  setDeleteAction(value: boolean) {
    this.deleteAction = value;
  }

  setActionAndClose(action: 'edit' | 'delete') {
    this.dialogRef.close(action);
  }
}
