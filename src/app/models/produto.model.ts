import { UnidadeMedida } from '../enums/unidade-medida.enum';

export class Produto {
    constructor(
        public nome: string,
        public unidadeMedida: UnidadeMedida,
        public quantidade: string,
        public preco: string,
        public perecivel: boolean,
        public dataValidade: string,
        public dataFabricacao: string,
        public id: string = null
    ){}
}