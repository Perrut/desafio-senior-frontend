import { Injectable } from '@angular/core';
import { UnidadeMedida } from '../enums/unidade-medida.enum';
import { Produto } from '../models/produto.model';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private tableName = 'produtos';

  constructor() {
    this.initializeDb();
  }

  initializeDb() {
    const data = localStorage.getItem(this.tableName);
    if (!data) {
      localStorage.setItem(this.tableName, '[]');
    }
  }

  saveProduct(produto: Produto) {
    produto.id = uuidv4();
    const produtos = this.getAllProducts();
    produtos.push(produto);
    localStorage.setItem('produtos', JSON.stringify(produtos));
  }

  findById(id: string) {
    return this.getAllProducts().find(p => p.id === id);
  }

  update(produto: Produto) {
    const produtos = this.getAllProducts();
    const index = produtos.indexOf(this.findById(produto.id));
    produtos.splice(index, 1);
    produtos.push(produto);
    localStorage.setItem('produtos', JSON.stringify(produtos));
  }

  delete(produto: Produto) {
    const produtos = this.getAllProducts();
    const index = produtos.indexOf(this.findById(produto.id));
    produtos.splice(index, 1);
    localStorage.setItem('produtos', JSON.stringify(produtos));
  }

  getAllProducts(): Produto[] {
    return JSON.parse(localStorage.getItem(this.tableName))
      .map(p => new Produto(p.nome, p.unidadeMedida,
        p.quantidade, p.preco, Boolean(p.perecivel), p.dataValidade, p.dataFabricacao, p.id));
  }
}
