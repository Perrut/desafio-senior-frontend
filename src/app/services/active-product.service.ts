import { Injectable } from '@angular/core';
import { Produto } from '../models/produto.model';

@Injectable({
  providedIn: 'root'
})
export class ActiveProductService {

  activeProduct: Produto;

  constructor() { }
}
