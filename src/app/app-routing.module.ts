import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormularioComponent } from './pages/formulario/formulario.component';
import { ListagemComponent } from './pages/listagem/listagem.component';

const routes: Routes = [
  { path: '', component: ListagemComponent },
  { path: 'formulario', component: FormularioComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
