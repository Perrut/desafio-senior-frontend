# SeniorFrontend

Desafio de frontend para o processo seletivo de desenvolvedor na empresa Senior

## Requisitos para a execução

- Node 10.x ou superior
- Angular CLI 10.x ou superior

## Execução

- A partir desta pasta, executar o comando:
```
npm install
```
- Em seguida
```
ng serve
```

## Considerações

- Tive um problema com a biblioteca PrimeNG, onde não conseguia importar uma estilização dos componentes de inputs de formulário, por essa razão, optei por usar o Angular Material.

- Caso não queira executar o projeto localmente, há uma versão no heroku que pode ser acessada através do link: https://arcane-scrubland-62522.herokuapp.com.

- Para testes no Edge versão 1 (que não é baseado no Chrome), recomendo usar a versão do Heroku, pois para executar o Angular em modo dev com esse navegador é necessária a instalação de polyfills.

- Assim como no outro desafio, gostaria de mais uma vez agradecer a oportunidade.
